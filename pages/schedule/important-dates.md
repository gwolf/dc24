---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 14rem;
}
</style>

| **APRIL**   |                                                                  |
|-------------|------------------------------------------------------------------|
| 15 (Monday) | Opening of the [Call For Proposals](/cfp/).                      |
| 15 (Monday) | Opening attendee registration.                                   |

| **MAY**       |                                                                                                                         |
|---------------|-------------------------------------------------------------------------------------------------------------------------|
| 1 (Wednesday) | Last date to let us know if you need a visa to South Korea (email: visa@debconf.org with a copy of your passport).      |
| 8 (Wednesday) | Last day for submitting a talk that will be surely considered for the bursary analysis.                                 |
| 8 (Wednesday) | Last date to apply for a bursary.                                                                                       |
| 23 (Thursday) | Last day for bursary team review travel requests and make 1st round of offers to bursary recipients.                    |

| **JUNE**      |                                                              |
|---------------|--------------------------------------------------------------|
| 2 (Sunday)    | Last day for submitting a talk that will be surely considered for the official schedule (early submission deadline).    |
| 7 (Thursday)  | Last day for 1st round of bursary recipients to accept.      |
| 7 (Thursday)  | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |

| **JULY**       |                                                  |
|----------------|--------------------------------------------------|
| *DebCamp*      |                                                  |
| 20 (Saturday)  | Arrival day for DebCamp                          |
| 21 (Sunday)    | First day of DebCamp                             |
| 22 (Monday)    | Second day of DebCamp                            |
| 23 (Tuesday)   | Third day of DebCamp                             |
| 24 (Wednesday) | Fourth day of DebCamp                            |
| 25 (Thursday)  | Fifth day of DebCamp                             |
| 26 (Friday)    | Sixth day of DebCamp                             |
| 27 (Saturday)  | Seventh day of DebCamp / Arrival day for DebConf |
| *DebConf*      |                                                  |
| 28 (Sunday)    | First day of DebConf / Debian Day                |
| 29 (Monday)    | Second day of DebConf                            |
| 30 (Tuesday)   | Third day of DebConf                             |
| 31 (Wednesday) | Fourth day of DebConf / Day Trip                 |

| **AUGUST**  |                                                             |
|----------------|----------------------------------------------------------|
| 01 (Thursday)  | Fifth day of DebConf / Conference Dinner                 |
| 02 (Friday)    | Sixth day of DebConf                                     |
| 03 (Saturday)  | Seventh day of DebConf                                   |
| 04 (Sunday)    | Last day of DebConf                                      |
| 05 (Monday)    | Departure day                                            |
