---
name: COVID-19
---

# COVID-19 Information (as of Mar 2024)

<br>

## Entry requirements

Since June 2022, Republic of Korea no longer imposes any COVID-19 related entry
requirements.

Airlines may have restrictions in place.

## Conference Policy

The DebConf24 organizers recommend regular testing policy and usage of face
masks. We recommend attendees to wear masks even if you don't show any symptoms,
for your own safety, especially when packed in confined spaces with many people.

## COVID-19 Testing

COVID-19 Antigen self test kits and face masks will be available at Front desk
upon requests.

All attendees should self-monitor their health post arrival. Any attendee having
symptoms of COVID-19 shall self-isolate as per standard protocol i.e. the said
attendee should be wearing mask, isolated and segregated from other attendees
and seek medical care as per health protocols.

## Isolation on site

Facilities for self isolation can be arranged at the nearby hotel.
